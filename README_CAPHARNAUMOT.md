Capharnaumot
===========

Développé par DELIN Noé, FOURMAINTRAUX Camille
Contacts : noe.delin.etu@univ-lille.fr, camille.fourmaintraux.etu@univ-lille.fr

# Présentation de Capharnaumot

Capharnaumot est un jeu de Tile-Matching simplifié. Il faut échanger des cases afin d'assembler des mots en fonction de leur famille.
Les couleurs avec les couleurs, les légumes avec les légumes, etc...
Le jeu est composé de plusieurs modes : le High Score runner, le Lingo Clash, et l'Objective Words.
Des captures d'écran illustrant le fonctionnement du logiciel sont proposées dans le répertoire shots.


# Utilisation de Capharnaumot

Afin d'utiliser le projet, il suffit de taper les commandes suivantes dans un terminal :

```
./compile.sh
```
Permet la compilation des fichiers présents dans 'src' et création des fichiers '.class' dans 'classes'

```
./run.sh capharnaumot
```
Permet le lancement du jeu
Le programme commence par un menu principal, par lequel le choix des differents modes de jeu s'offre au joueur, ainsi que la possibilité de visualiser les differentes tables de scores.
Le programme crée une grille de mots aléatoires, puis la stabilise en faisant disparaitre les alignements de mots de même catégories.
Il faut entrer "SAVE" pour sauvegarder une partie en cours. Pour quitter le programme, il faut selectionner l'option 5 du menu principals.