import extensions.CSVFile;
import extensions.File;

class TestCSV extends Program{
    void algorithm(){
        String[][] game = loadGame("Test_Sauvegarde_Grille.csv");
        
    }
    
    Mot newMot(String chaine, String categorie){
        Mot mot = new Mot();
        mot.chaine = chaine;
        mot.categorie = categorie;
        return mot;
    }

    // Transforme un type Mot en chaine de caractères.
    String motToString(Mot mot){
        return mot.chaine + " " + mot.categorie;
    }
    void testMotToString(){
        Mot m = newMot("acacia", "ARBRE");
        assertEquals("acacia ARBRE",motToString(m));
    }


    // Transforme une chaine de caractères en type Mot.
    Mot stringToMot(String cell){
        int i = 0;
        while(charAt(cell,i)!=' '){
            i++;
        }
        Mot m = new Mot();
        m.chaine = substring(cell,0,i);
        m.categorie = substring(cell,i+1,length(cell));
        return m;
    }
    void testStringToMot(){
        Mot m = stringToMot("bleu COULEUR");
        assertEquals("bleu",m.chaine);
        assertEquals("COULEUR",m.categorie);
        
    }
    
    
    Joueur newJoueur(int idJ, String nom){
        Joueur j = new Joueur();
        j.idJ = idJ;
        j.nom = nom;
        return j;
    }


    // Transforme un type Joueur en chaine de caractères.
    String joueurToString(Joueur j){
        return j.idJ + " " + j.nom;
    }
    void testJoueurToString(){
        Joueur j1 = newJoueur(1,"Arthur");
        Joueur j2 = newJoueur(29,"Bô");
        assertEquals("1 Arthur", joueurToString(j1));
        assertEquals("29 Bô", joueurToString(j2));
    }


    // Transforme une chaine de caractères en type Joueur.
    Joueur stringToJoueur(String chaine){
        int space = 0;
        while(charAt(chaine,space)!=' '){
            space++;
        }
        Joueur j = new Joueur();
        j.idJ = stringToInt(substring(chaine,0,space));
        j.nom = substring(chaine, space+1, length(chaine));
        return j;
    }
    void testStringToJoueur(){
        String chaine = "2 Shell";
        Joueur j = stringToJoueur(chaine);
        assertEquals(2,j.idJ);
        assertEquals("Shell",j.nom);
    }


    // Tri les joueurs par idJ et les sauvegarde dans un fichier CSV.
    void saveJoueursCSV(Joueur[] joueurs){
        String[][] j = new String[length(joueurs)][1];
        for(int i = 0; i < length(joueurs); i++){
            j[i][0] = joueurToString(joueurs[i]);
        }
        saveCSV(j,"../ressources/infosJoueurs/liste_des_joueurs.csv");
    }
    
    // Charge dans un tableau la liste de joueurs.
    Joueur[] loadJoueurCSV(){
        CSVFile j = loadCSV("../ressources/infosJoueurs/liste_des_joueurs.csv");
        Joueur[] joueurs = new Joueur[rowCount(j)];
        for(int i = 0; i < length(joueurs); i++){
            joueurs[i] = stringToJoueur(getCell(j,i,0));
        }
        return joueurs;
    }

    String saisieIDJoueur(Joueur[] joueursListe){
        String saisie;
        do{
            saisie = readString();
        }while(!saisieIDJoueurValide(saisie, joueursListe));
        return saisie;
    }
    
    boolean saisieIDJoueurValide(String saisie, Joueur[] joueursListe){
        boolean valide = true;
        if(!equals(saisie,"")){
            int cpt = 0;
            while(valide && cpt < length(saisie)){
                valide = (charAt(saisie,cpt) >= '0') || (charAt(saisie,cpt) <= '9');
                cpt++;
            }
            if(valide){
                valide = (stringToInt(saisie) >= joueursListe[0].idJ) && (stringToInt(saisie) <= joueursListe[length(joueursListe)-1].idJ);
            }
        }
        return valide;
    }

    Joueur creerNouveauJoueur(Joueur[] joueursListe){

        clearScreen();
        delay(500);
        Joueur[] nouvelleListeDeJoueurs = new Joueur[length(joueursListe)+1];
        Joueur j = new Joueur();
        j.idJ = joueursListe[length(joueursListe)-1].idJ+1;
        print("Entrez un pseudo : ");
        j.nom = readString();

        for(int i = 0; i < length(joueursListe); i++){

            nouvelleListeDeJoueurs[i] = joueursListe[i];

        }

        nouvelleListeDeJoueurs[length(nouvelleListeDeJoueurs)-1] = j;
        joueursListe = nouvelleListeDeJoueurs;
        return j;
        
    }
    
    void affichageListeJoueurs(Joueur[] joueursListe){

        println("--------------------");

        for(int i = 0; i < length(joueursListe); i++){

            println("|  " + joueursListe[i].idJ + " : " + joueursListe[i].nom);

        }
        
        println("--------------------");
    }

    Joueur menuIdentificationJoueur(Joueur[] joueursListe){

        clearScreen();
        affichageListeJoueurs(joueursListe);
        println();
        println("Identifiez-vous. Selectionnez votre numéro.");
        println("Si vous nouveau faite [entrer] sans rien écrire pour accéder au menu d'inscription");
        print("==> ");
        String numJoueurChaine = saisieIDJoueur(joueursListe); // à changer pour éviter le crash.

        if(equals(numJoueurChaine,"")){

            // créer un nouveau joueur.
            return creerNouveauJoueur(joueursListe);

        }else{
            
            // choisir un joueur déjà dans la liste.
            int numJoueur = stringToInt(numJoueurChaine);
            return joueursListe[numJoueur-1];

        }

    }

    // Convertit un nombre en String en Int. /!\ Les caractères doivent donc être compris entre 0 et 9.
    int StringToInt(String nombre){
        int total=0;
        for(int i=0; i<length(nombre);i+=1){
            total=total+(int)((charAt(nombre,i)-'0')*(pow(10.0,length(nombre)-(i+1))));
        }
        return total;
    }

    // Verifie si un dossier ciblé est vide ou non.
    boolean estDossierVide(String dossier){
        String[] contenu = getAllFilesFromDirectory(dossier);
        return length(contenu) == 0;
    }
    void testEstDossierVide(){
        String dossier1 = "../ressources/grilles_jeux_sauvegardes/Plus_de_points_possible";
        assertTrue(estDossierVide(dossier1));

        String dossier2 = "..";
        assertFalse(estDossierVide(dossier2));
    }

    String dimensionToString(Mot[][] grille){
        return length(grille,1) + " " + length(grille,2);
    }

    // Sauvegarde une partie en cours [Mode 1 : plus de points possible].
    void saveGameMode1(Mot[][] grille, Joueur joueurCourrant,int score, int coupsRestants){
        String[][] sauvegarde = new String[length(grille,1)][length(grille,2)+1];
        for(int i = 0; i < length(grille,1); i++){
            for(int j = 0; j < length(grille,2); j++){
                sauvegarde[i][j] = motToString(grille[i][j]);
            }
        }
        sauvegarde[0][length(sauvegarde,2)-1] = dimensionToString(grille);
        sauvegarde[1][length(sauvegarde,2)-1] = joueurToString(joueurCourrant);
        sauvegarde[2][length(sauvegarde,2)-1] = "" + score;
        sauvegarde[3][length(sauvegarde,2)-1] = "" + coupsRestants;
        saveCSV(sauvegarde, "Test_Sauvegarde_Grille.csv");
    }

    String[][] loadGame(String nomFichierCSV){
        CSVFile f = loadCSV(nomFichierCSV);
        String[][] partie = new String[rowCount(f)][columnCount(f)];
        for(int i = 0; i < length(partie,1); i++){
            for(int j = 0; j < length(partie,2); j++){
                partie[i][j] = getCell(f,i,j);
            }
        }
        return partie;
    }
    
    


    // Initialise la grille avec des mots aléatoire
    void initialiserGrille(Mot[][] grille, CSVFile f){
        for (int l = 0; l<length(grille,1); l+=1){
            for (int c = 0; c<length(grille,2); c+=1){
                grille[l][c]=newMotAlea(f);
            }
        }
    }
    // Fonction intestable car aléatoire
    Mot newMotAlea(CSVFile f){
        Mot mot = newMot(pioche(columnCount(f)),pioche(rowCount(f)-1)+1,f);
        return mot;
    }
    // Cree une grille de mot en fonction de deux entiers (la longueur et la largeur) passé en paramètre.
    Mot[][] creerGrille(int longueur, int largeur){
        return new Mot[longueur][largeur];
    }
    // tire un entier au hasard entre 0 et max exlus
    int pioche(int max){
        return (int) (random()*max);
    }
    Mot newMot(int colonne, int ligne, CSVFile f){
        Mot mot = new Mot();
        mot.chaine = getCell(f, ligne, colonne);
        mot.categorie = getCell(f, 0, colonne);
        return mot;
    }
}