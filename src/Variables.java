import extensions.CSVFile;
class Variables{
    int score;
    int nbCoupsRestants;
    int nbCoupsEffectues;
    int longueur;
    int largeur;
    int max;
    CSVFile mots;
    String fichierDePartie;
    Mot[][] grille;
    String saisie;
    Joueur[] joueurs; // Pour le mode Lingo Clash
    int[] coordonnees;
    boolean partieDejaExistante;

}